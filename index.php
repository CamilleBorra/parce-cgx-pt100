<?php require_once "bdd.php"?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap.min.css" type="text/css"> 
    <link rel="stylesheet" href="style.css" type="text/css"> 
    <title>parse</title>
</head>
<body>
    <div class="container">
        <section id="upload">
            <form action="index.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <h2>Mettez votre fichier Json</h2>
                    <label for="fileUpload">Fichier:</label>
                    <input type="file" name="photo" id="fileUpload">
                    <input type="submit" name="submit" value="Générer le graphe">
                    <p><strong>Note:</strong> Seul le format .json est autorisé.</p>
                </div>
            </form>
        </section>

        <section id="chart">
            <canvas id="myChart" width="100%"></canvas>

        </section>
        

    </div>
    <script src="jquery.js"></script>
    <script src="chart.min.js"></script>
   
   
    <?php
        if(isset($_POST['submit'])){
            $files = glob('fichierJson/*'); // get all file names
            foreach($files as $file){ // iterate files
            if(is_file($file))
            unlink($file); // delete file
}
            // Vérifie si le fichier a été uploadé sans erreur.
            if(isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0){
                $allowed = array("json" => "application/json");
                $filename = $_FILES["photo"]["name"];
                $filetype = $_FILES["photo"]["type"];
                $filesize = $_FILES["photo"]["size"];
        
                // Vérifie l'extension du fichier
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if(!array_key_exists($ext, $allowed)) die("Erreur : Veuillez sélectionner un format de fichier valide.");
        
                // Vérifie le type MIME du fichier
                if(in_array($filetype, $allowed)){
                    // Vérifie si le fichier existe avant de le télécharger.
                        $PT1Tab=[];
                        $PT2Tab=[];
                        $timeTab=[];
                        move_uploaded_file($_FILES["photo"]["tmp_name"], "fichierJson/" . $_FILES["photo"]["name"]);
                        echo "Votre fichier a été téléchargé avec succès.";
                        $jsonTotal=file_get_contents('fichierJson/'.$_FILES["photo"]["name"]);
                        $jsonTotal=json_decode($jsonTotal, true);
                        for($i=0;$i<count($jsonTotal);$i++)
                        {
                            if(isset($jsonTotal[$i]['data']['content']['CGA']['CAPTEURS']))

                            {
                                
                                $time=$jsonTotal[$i]['data']['date'];
                                
                                $explode=explode('.',$time);
                                $time=$explode[0];
                                $exist=0;
                                $site=$db->query("Select * FROM site WHERE codeAffaire=1 ");
                                while($siteExe=$site->fetch()){
                                    $seuilIntensite=$siteExe['seuilIntensite'];
                                    $seuilVent=$siteExe['seuilVent'];
                                }
                                $existTime=$db->query("Select * FROM log WHERE dateLog=\"$time\" AND codeAffaire=1");
                                while($existTimeExe=$existTime->fetch()){
                                    if($existTimeExe['dateLog']==$time)
                                    {
                                        $exist=1;
                                    }
                                    else{
                                        $exist=0;
                                    }
                                }
                                
                                
                                $PT1 = $jsonTotal[$i]['data']['content']['CGA']['CAPTEURS'][0]['Temperatures']['PT100_1']['Avg'];
                                $PT2 = $jsonTotal[$i]['data']['content']['CGA']['CAPTEURS'][0]['Temperatures']['PT100_2']['Avg'];
                                
                                if ($exist==0)
                                {
                                    $insertData=$db->query("INSERT INTO log (dateLog,intensiteLog,ventLog, codeAffaire) Values (\"$time\",$PT1,$PT2,1)");
                                    if($PT1<$seuilIntensite || $PT2>$seuilVent){
                                        $insertProbleme=$db->query("INSERT INTO probleme (vent,electricite,dateProbleme,codeAffaire,idCategorie) VALUES ($PT2,$PT1,\"$time\",1,1)");
                                    }
                                    
                                }
                                
                            }
                            
                            
                            //var_dump($jsonTotal[1]['data']['content']['CGA']['CAPTEURS'][0]['Temperatures']['PT100_1']['Avg']);
                            
                        }

                        ?>
                        <script>
                                var zone1=[];
                                var zone2=[];
                                
                                $.ajax({
                                    url: "fichierJson/<?php echo $_FILES["photo"]["name"]?>",
                                    method: 'GET',
                                    success: function(event) {
                                        
                                        //const fileJson = JSON.parse(event);
                                        console.log("success")
                                        //console.log(event);
                                        //console.log(event[0]['data']['content']['CGA']['CAPTEURS'][0]['Temperatures']['PT100_1'])

                                        var dateTab=[];
                                        var time;
                                        var date;
                                        var dateJourTab=[];
                                        var dateJour;
                                        var datePass=[];
                                        var datePass2=[]
                                        var recap=document.getElementById("recap");
                                        for(var i=2;i<event.length;i++)
                                        {   //console.log(event.length)
                                            //console.log(i)
                                           // console.log(event[3]['data']['content']['CGA']['CAPTEURS'][0]['Temperatures'])
                                            console.log(event[2]['data']['content']['CGA']['CAPTEURS'][0]['Temperatures']['PT100_1']['Avg'])
                                            if (event[i]['data']['content']['CGA']['CAPTEURS'])
                                            {
                                            // console.log(event[i]['timestamp']);
                                                time=event[i]['timestamp'];
                                                var timeTimestamp;
                                                timeTimestamp=time;
                                                var dayText;
                                                time=new Date (time);
                                                day=time.getDay();
                                                console.log(day);
                                                switch (day) {
                                                    case 0:
                                                        dayText="Dimanche"
                                                        break;
                                                    case 1:
                                                        dayText="Lundi"
                                                        break;
                                                    case 2:
                                                        dayText="Mardi"
                                                        break;
                                                    case 3:
                                                        dayText="Mercredi"
                                                        break;
                                                    case 4:
                                                        dayText="Jeudi"
                                                        break;
                                                    case 5:
                                                        dayText="Vendredi"
                                                        break;
                                                    case 6:
                                                        dayText="Samedi"
                                                        break;
                                                        default:
                                                        dayText="Default";   
                                                    
                                                    
                                                }
                                                
                                                dateJour=dayText+" "+time.getDate()+"/"+(time.getMonth()+1)+"/"+time.getFullYear()
                                                date=time.getDate()+"/"+(time.getMonth()+1)+"/"+time.getFullYear()+" - "+time.getHours()+":"+time.getMinutes()+":"+time.getSeconds();
                                                
                                                dateTab.push(date);
                                                dateJourTab.push(dateJour);
                                                
                                                //console.log(hourTab);
                                                //console.log(dateTab);


                                                        zone1.push(event[i]['data']['content']['CGA']['CAPTEURS'][0]['Temperatures']['PT100_1']['Avg']);
                                                        for(var s=0; s<dateJourTab.length;s++)
                                                            {
                                                                datePass[s]=[];
                                                                datePass[s][0]=dateJourTab[s];
                                                                datePass[s][1]=zone1[s];
                                                                
                                                            }
                                                        zone2.push(event[i]['data']['content']['CGA']['CAPTEURS'][0]['Temperatures']['PT100_2']['Avg'])
                                                        for(var s=0; s<dateJourTab.length;s++)
                                                            {
                                                                datePass2[s]=[];
                                                                datePass2[s][0]=dateJourTab[s];
                                                                datePass2[s][1]=zone2[s];
                                                                
                                                            }
                                                        
                                                        
                                                        
                                                        
                                                    
                                                //console.log(hourTab.len gth);    
                                                //console.log(zone2)
                                                //datePass[i][0]=dateJourTab[i];
                                                //datePass[i][1]=zone1[i];
                                            }
                                            //else {console.log("pas ok");}
                                        }
                                       var cumule;
                                       //console.log(datePass)
                                       for(var q=0;q<datePass.length;q++)
                                        {
                                            
                                            if (q!=0)
                                            {
                                                if(datePass[q][0]==datePass[q-1][0]&&q!=(datePass.length-1))
                                                {
                                                   cumule=cumule+datePass[q][1];
                                                   console.log("entrer if");
                                                }
                                                else 
                                                {
                                                    //recap.writeln("Le"+datePass[q-1][0]+" il y a eu "+cumule+" passages");
                                                    console.log(datePass[q-1][0]+" il y a eu "+cumule+" passages ")
                                                    $('#recap').append(datePass[q-1][0]+" il y a eu "+cumule+" passages dans le bon sens<br/>")
                                                    
                                                    cumule=datePass[q][1];
                                                    console.log("entrer else");
                                                }
                                                
                                            }
                                            else 
                                            {
                                                cumule=datePass[q][1];
                                            }
                                        }
                                        var cumule2
                                        for(var q=0;q<datePass2.length;q++)
                                        {
                                            
                                            if (q!=0)
                                            {
                                                if(datePass2[q][0]==datePass2[q-1][0]&&q!=(datePass2.length-1))
                                                {
                                                    cumule2=cumule2+datePass2[q][1];
                                                    console.log("entrer if");
                                                }
                                                else 
                                                {
                                                    //recap.writeln("Le"+datePass[q-1][0]+" il y a eu "+cumule+" passages");
                                                    console.log(datePass2[q-1][0]+" il y a eu "+cumule2+" passages")
                                                    $('#recap').append(datePass2[q-1][0]+" il y a eu "+cumule2+" passages à contresens <br/>")
                                                    
                                                    cumule2=datePass2[q][1];
                                                    console.log("entrer else");
                                                }
                                                
                                            }
                                            else 
                                            {
                                               
                                               cumule2=datePass2[q][1];
                                            }
                                        }
                                        
                                       
                                        new Chart(document.getElementById("myChart"), { 
                                            type: 'line', 
                                            data: {
                                                labels: dateTab,
                                                datasets: [{
                                                    label: 'PT100_1',
                                                    data: zone1,
                                                    borderColor: '#0080FF',
                                                    borderWidth:0.5,
                                                    fill: false,
                                                },
                                                {
                                                    label: 'PT100_2',
                                                    data: zone2,
                                                    borderColor: '#FF0000',
                                                    borderWidth:0.5,
                                                    fill: false,
                                                }]
                                            },
                                            options: {
                                                title: {
                                                display: true,
                                                text: 'Nombre de Passage'
                                                },
                                                pointRadius:0,
                                                
                                            }
                                        });
                                    },
                                    error: function(err) {
                                        console.error(err)
                                        console.log("error")
                                    }
                                })

                        </script>
                        <?php
                    
                } else{
                    echo "Error: Il y a eu un problème de téléchargement de votre fichier. Veuillez réessayer."; 
                }
            } else{
                echo "Error: " . $_FILES["photo"]["error"];
                var_dump("erreur php");
            }
            //header('Location: ');
        
        
        }
    ?>
    
</body>
</html>